docker kill app
docker rm app

docker run -p 3000:3000 \
    -ti \
    --name app \
    -w /app --mount type=bind,src="$(pwd)",target=/app \
    node:18 \
    sh -c "npm install && npm start"
