import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Artist} from "./Artist";
import {Track} from "./Track";

@Entity("albums")
export class Album {
    @PrimaryGeneratedColumn()
    AlbumId: number;

    @Column()
    Title: string;

    @ManyToOne(type => Artist, artist => artist.Albums)
    @Column("integer", {name: "ArtistId"})
    @JoinColumn({ name: "ArtistId" })
    Artist: Artist

    @OneToMany(type => Track, t => t.Album)
    Tracks: Track[]
}

