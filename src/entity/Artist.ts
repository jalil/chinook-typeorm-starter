import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Album} from "./Album";

@Entity("artists")
export class Artist {
    @PrimaryGeneratedColumn()
    ArtistId: number;

    @Column()
    Name: string;

    @OneToMany(() => Album, album => album.Artist)
    Albums: Album[]
}
