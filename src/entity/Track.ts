import {Column, Entity, JoinColumn, ManyToOne,PrimaryGeneratedColumn} from "typeorm";
import {Album} from "./Album";

@Entity("tracks")
export class Track {
    @PrimaryGeneratedColumn()
    TrackId: number;

    @Column()
    Name: string;

    @ManyToOne(() => Album)
    @JoinColumn({ name: "AlbumId" })
    @Column("integer", {name: "AlbumId"})
    Album: Album
}
