import { AppDataSource } from "./data-source"
import { Album } from "./entity/Album";

AppDataSource.initialize().then(async () => {
    console.log("Album avec leurs artistes et leurs morceaux")
    const albums = await AppDataSource.manager.find(Album, {relations:["Tracks", "Artist"]})
    const toPrint = albums
        .map(album => ({
            Title: album.Title,
            Artist: album.Artist.Name,
            Tracks: album.Tracks.map(track => track.Name)
        }))
    console.log(JSON.stringify(toPrint, null, 2))
    console.log(`${toPrint.length} albums !`)
}).catch(error => console.log(error))
