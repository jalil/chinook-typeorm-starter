import "reflect-metadata"
import { DataSource } from "typeorm"
import { Track } from "./entity/Track";
import { Album } from "./entity/Album";
import { Artist } from "./entity/Artist";

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "chinook.db",
    synchronize: false,
    logging: false,
    entities: [Track, Album, Artist],
    migrations: [],
    subscribers: [],
})
